package PrototypePattern;

public class CloneFactory {

    public AutoMobil getClone(AutoMobil autoMobil) {
        return autoMobil.makeCopy();
    }
}

