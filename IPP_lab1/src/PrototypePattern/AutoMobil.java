package PrototypePattern;

public interface AutoMobil extends Cloneable {
    AutoMobil makeCopy();
}
