package PrototypePattern;

public class Mercedes implements AutoMobil {
    public AutoMobil makeCopy() {
        System.out.println("Mercedes...");
        Mercedes sheepObject = null;
        try {
            sheepObject = (Mercedes) super.clone();
        }
        catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return sheepObject;
    }
    public String toString(){
        return "Car Mercedes";
    }
}
