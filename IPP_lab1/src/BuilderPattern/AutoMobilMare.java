package BuilderPattern;

public class AutoMobilMare implements AutoMobilBuilder {
    private AutoMobil autoMobil;

    public AutoMobilMare() {
        autoMobil = new AutoMobil();
    }

    @Override
    public void setDenumire(String denumire) {
        autoMobil.setDenumire(denumire);
    }

    @Override
    public void setNrUsi(int nrUsi) {
        autoMobil.setNrUsi(nrUsi);
    }

    @Override
    public void setCuloare(String culoare) {
        autoMobil.setCuloare(culoare);
    }

    @Override
    public void setCapacitate(int capacitate) {
        autoMobil.setCapacitate(capacitate);
    }

    @Override
    public AutoMobil caracteristica() {
        return autoMobil;
    }
}

