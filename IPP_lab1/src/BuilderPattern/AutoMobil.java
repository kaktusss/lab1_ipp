package BuilderPattern;

public class AutoMobil {
    private int nrUsi;
    private String culoare;
    private int capacitate;
    private String denumire;

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public void setNrUsi(int nrUsi) {
        this.nrUsi = nrUsi;
    }

    public String getCuloare() {
        return culoare;
    }

    public void setCuloare(String culoare) {
        this.culoare = culoare;
    }

    public void setCapacitate(int capacitate) {
        this.capacitate = capacitate;
    }

    public String toString() {
        return "Car: " + denumire + ", "
                + culoare + ", " + nrUsi + ", " +capacitate;
    }
}

