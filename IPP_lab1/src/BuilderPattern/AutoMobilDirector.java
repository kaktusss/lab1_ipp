package BuilderPattern;

public class AutoMobilDirector {
    private AutoMobilBuilder builder;

    public AutoMobilDirector(AutoMobilBuilder builder) {
        this.builder = builder;
    }

    public AutoMobil construct() {
        builder.setDenumire("Mercedes");
        builder.setCuloare("Red");
        builder.setCapacitate(40);
        builder.setNrUsi(4);
        return builder.caracteristica();
    }
}
