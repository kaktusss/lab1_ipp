package BuilderPattern;

public interface AutoMobilBuilder {
    void setDenumire(String denumire);
    void setNrUsi(int nrUsi);
    void setCuloare(String culoare);
    void setCapacitate(int capacitate);

    AutoMobil caracteristica();

}

