package AbstractFactory;

public class AutoMobilMare extends Masina {
    private String denumire;
    private int capacitatea;


    public AutoMobilMare(int viteza, String culoare, String denumire, int capacitate) {
        super(viteza, culoare);
        this.denumire = denumire;
        this.capacitatea = capacitate;
    }

    @Override
    public String ArataCaracteristi() {
        return denumire + ", " + capacitatea + ", "
                + getViteza() + ", " + getCuloare();
    }
}
