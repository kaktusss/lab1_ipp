package AbstractFactory;

public interface AbstractFabrica {
    Masina createMasina();
    AbstractFactory.Avion createAvion();
}
