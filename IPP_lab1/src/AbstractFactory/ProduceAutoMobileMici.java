package AbstractFactory;

import static java.lang.System.out;

public class ProduceAutoMobileMici implements AbstractFabrica{
    @Override
    public Masina createMasina() {
        out.print("Factory for the production car:  ");
        return new AutoMobilMic("Lada", 20, 120, "Black");
    }

    @Override
    public Avion createAvion() {
        out.print("Factory for the production airplane: ");
        return new AvionMic(50, "Blue/Red", 500, "Tu-40");
    }
}
