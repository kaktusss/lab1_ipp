package AbstractFactory;

public class AvionMare extends Avion {
    private String denumire;

    public AvionMare(int viteza, String culoare, int altitudine, String denumire) {
        super(viteza, culoare, altitudine);
        this.denumire = denumire;
    }

    @Override
    public String ArataCaracteristi() {
        return denumire + ", " + getViteza()+ ", "
                + getCuloare()+ ", " + getAltitudine();
    }
}

