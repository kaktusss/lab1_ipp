package AbstractFactory;

public class AutoMobilMic extends Masina {
    private String denumire;
    private int capacitatea;
    public static final String SPACE = ", ";

    public AutoMobilMic(String denumire, int capacitatea, int viteza, String culoare) {
        super(viteza, culoare);
        this.denumire = denumire;
        this.capacitatea = capacitatea;
    }

    @Override
    public String ArataCaracteristi() {
        return denumire + SPACE + capacitatea + ", "
                + getCuloare() + ", " + getViteza();
    }

}

