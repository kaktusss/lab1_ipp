package AbstractFactory;

import static java.lang.System.out;

public class ProduceAutoMobileMari  implements AbstractFabrica{
    @Override
    public Masina createMasina() {
        out.print("Factory for the production car:  ");
        return new AutoMobilMare(80, "Red", "Volvo", 500);
    }

    @Override
    public Avion createAvion() {
        out.print("Factory for the production airplane: ");
        return new AvionMare(200, "White", 5000, "AirBus");
    }
}

