package AbstractFactory;

public abstract class Avion {
    private int viteza;
    private String culoare;
    int altitudine;

    public Avion(int viteza, String culoare, int altitudine) {
        this.viteza = viteza;
        this.culoare = culoare;
        this.altitudine = altitudine;
    }

    public int getViteza() {
        return viteza;
    }

    public String getCuloare() {
        return culoare;
    }

    public int getAltitudine() {
        return altitudine;
    }

    public abstract String ArataCaracteristi();
}
