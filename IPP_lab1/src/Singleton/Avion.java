package Singleton;

public class Avion {
    private String denumire;
    private int nrLocuri;
    private String culoare;
    private int altitudine;
    private static Avion avion;

    private Avion(String denumire, int nrLocuri, String culoare, int altitudine) {
        this.denumire = denumire;
        this.nrLocuri = nrLocuri;
        this.culoare = culoare;
        this.altitudine = altitudine;
    }
    public static Avion getInstance() {
        if (avion == null) {
            avion = new Avion("AirBus", 200, "White", 2000);
        }
        return avion;
    }
    public String afiseazaCarateristica() {
        return "Airplane: " + denumire + ", " + nrLocuri + ", "
                + culoare + ", " + altitudine;
    }
}
