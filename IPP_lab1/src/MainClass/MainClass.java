package MainClass;

import AbstractFactory.*;
import BuilderPattern.AutoMobilBuilder;
import AbstractFactory.Avion;
import BuilderPattern.*;
import BuilderPattern.AutoMobilMare;
import FactoryMethod.ProduceAutomobile;
import PrototypePattern.CloneFactory;
import PrototypePattern.Mercedes;

public class MainClass {
    public static void main(String[] args) {
        AbstractFabrica abstractFabrica = new ProduceAutoMobileMari();
        System.out.print("Abstract Factory Design Pattern. "); // Abstract Factory Design Pattern.
        System.out.println("Manufacturer car/airplane Large.....");
        ArataProducatoriMasini(abstractFabrica);
        System.out.print("\n");
        System.out.print("Abstract Factory Pattern. ");
        System.out.println("Manufacturer car/airplane Small.....");
        abstractFabrica = new ProduceAutoMobileMici();
        ArataProducatoriMasini(abstractFabrica);

        System.out.print("\n"); // Factory Method Design Pattern
        System.out.println("Factory Method Pattern.....");
        ProduceAutomobile automobile = new FactoryMethod.ProduceAutoMobileMari();
        FactoryMethod.AutoMobilMare mobilMare = automobile.createAutoMobilMare("Volvo");
        System.out.println(mobilMare.ArataCaracteristici());
        mobilMare = automobile.createAutoMobilMare("Mercedes");
        System.out.println(mobilMare.ArataCaracteristici());

        System.out.print("\n");
        System.out.println("Builder Pattern....."); // Builder pattern
        AutoMobilBuilder builder = new AutoMobilMare();
        AutoMobilDirector autoMobilDirector = new AutoMobilDirector(builder);
        System.out.print(autoMobilDirector.construct());

        System.out.println("\n");
        System.out.println("Singleton Pattern..... "); //Singleton Pattern
        Singleton.Avion airplane = Singleton.Avion.getInstance();
        System.out.print(airplane.afiseazaCarateristica());

        System.out.println("\n");
        System.out.println("Prototype Pattern...");  // Prototype Pattern
        CloneFactory factory = new CloneFactory();
        Mercedes mercedes = new Mercedes();
        Mercedes clonedSheep = (Mercedes) factory.getClone(mercedes);
        System.out.println(mercedes);
        System.out.println(clonedSheep);
    }

    private static void ArataProducatoriMasini(AbstractFabrica abstracFabrica) {
        Masina masina = abstracFabrica.createMasina();
        System.out.println(masina.ArataCaracteristi());

        Avion avion = abstracFabrica.createAvion();
        System.out.println(avion.ArataCaracteristi());
    }

}

