package FactoryMethod;

public interface ProduceAutomobile {
    AutoMobilMare createAutoMobilMare(String denumire);
}

