package FactoryMethod;

import java.util.InputMismatchException;

public class ProduceAutoMobileMari implements ProduceAutomobile {
    @Override
    public AutoMobilMare createAutoMobilMare(String denumire) {
        if (denumire.equals("Volvo")) {
            return new Volvo("Volvo", "White", "20 Tone", "152 KM");
        }
        if (denumire.equals("Mercedes")) {
            return new Mercedes("Mercedes", "Black", "25 Tone", "140 Km");
        }
        throw new InputMismatchException("There is no such brand of Car: " +denumire);
    }
}

