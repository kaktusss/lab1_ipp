package FactoryMethod;

public class Volvo implements AutoMobilMare {
    private String denumire;
    private String culoare;
    private String capacitate;
    private String viteza;


    public Volvo(String denumire, String culoare, String capacitate, String viteza) {
        this.denumire = denumire;
        this.culoare = culoare;
        this.capacitate = capacitate;
        this.viteza = viteza;
    }

    @Override
    public String ArataCaracteristici() {
        return "Car: " + denumire + ", "
                + culoare + ", " + capacitate + ", " + viteza;

    }
}
