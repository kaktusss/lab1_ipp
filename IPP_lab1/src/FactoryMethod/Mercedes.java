package FactoryMethod;

public class Mercedes implements AutoMobilMare {
    private String denumire;
    private String culoare;
    private String capacitate;
    private String viteza;


    public Mercedes(String denumire, String culoare, String capacitate, String viteza) {
        this.denumire = denumire;
        this.culoare = culoare;
        this.capacitate = capacitate;
        this.viteza = viteza;
    }

    @Override
    public String ArataCaracteristici() {
        return "Car: " + denumire + ", "
                + culoare + ", " + capacitate + ", " + viteza;

    }
}

