package FacadePattern;

import static java.lang.System.out;

public class DepositsBank {
    private Deposits sonata = new SonataDep(14, "Sonata");
    private Deposits tango = new TangoDep(9, "Tango");
    private Deposits jazz = new JazzDep(3, "Jazz");

    public String informJazz(){
        return jazz.infoDeposits();
    }

    public String informSonata(){
        return sonata.infoDeposits();
    }

    public String informTango(){
        return tango.infoDeposits();
    }
}
