package FacadePattern;

public class TangoDep implements Deposits {
    private int sumIntr;
    private String name;
    private int percent;

    public TangoDep(int sumIntr, String name) {
        this.sumIntr = sumIntr;
        this.name = name;
        this.percent = 4000;
    }
    @Override
    public int calcPercent() {
        return percent / 100 * sumIntr;
    }

    public String getName() {
        return name;
    }

    public int getSumIntr() {
        return sumIntr;
    }

    @Override
    public String infoDeposits() {
        return " Deposit " + getName() +
                " offers " + calcPercent() + " from " + getSumIntr();
    }
}
