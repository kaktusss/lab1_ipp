package CompositePattern;

public class Creditor implements  Employed{
    private int nrBranch;
    private String name;

    public Creditor(int id, String name){
        this.nrBranch = id;
        this.name = name;
    }

    @Override
    public void add(Employed employed){

    }

    @Override
    public int getNrBranch() {
        return nrBranch;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void print(){
        System.out.print("Nr . Branch: " + getNrBranch());
        System.out.println("\tName: " + getName());
    }
}
