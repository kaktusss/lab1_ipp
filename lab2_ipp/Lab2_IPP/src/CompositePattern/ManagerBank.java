package CompositePattern;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ManagerBank implements Employed {
    private int nrBranch;
    private String name;

    public ManagerBank(int id, String name){
        this.nrBranch = id;
        this.name = name;
    }

    List<Employed> employeds = new ArrayList<>();
    @Override
    public void add(Employed employed){
        employeds.add(employed);
    }

    @Override
    public int getNrBranch() {
        return nrBranch;
    }

    @Override
    public String getName(){
        return name;
    }

    @Override
    public void print(){
        System.out.print("Nr. Branch: "+ getNrBranch());
        System.out.println("\tName: "+ getName());
        Iterator<Employed> it = employeds.iterator();
        while(it.hasNext()) {
            Employed employed = it.next();
            employed.print();
        }
    }
}
