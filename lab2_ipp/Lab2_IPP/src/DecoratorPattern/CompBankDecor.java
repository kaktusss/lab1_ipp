package DecoratorPattern;

public abstract class CompBankDecor implements CompBank{
    protected CompBank compBank;

    public CompBankDecor(CompBank compBank){
        this.compBank = compBank;
    }

    public String InfoBank(){
        return compBank.InfoBank();
    }


}
