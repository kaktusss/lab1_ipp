package DecoratorPattern;

public class NameBank implements CompBank {
    private String nameBank;

    public NameBank(){
        nameBank = "MobiasBank: ";
    }

    @Override
    public String InfoBank() {
        return nameBank;
    }
}
