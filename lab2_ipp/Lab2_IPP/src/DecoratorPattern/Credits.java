package DecoratorPattern;

public class Credits extends CompBankDecor {
    public String infoCredits = "Credit Section";

    public Credits(CompBank compBank){
        super(compBank);
    }

    public String InfoBank(){
        return compBank.InfoBank() + credits();
    }

    public String credits(){
        return infoCredits;
    }
}
