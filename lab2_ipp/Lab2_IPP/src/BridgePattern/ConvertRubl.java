package BridgePattern;

public class ConvertRubl implements Exchange {
    double rubl = 0.25;

    @Override
    public void convertVal(int numeral){
        System.out.println( + numeral + " Ruble = " + (numeral * rubl) + " Lei ");
    }
}