package Main;

import AdapterPattern.DetailBank;
import AdapterPattern.GetCredit;
import BridgePattern.*;
import CompositePattern.Creditor;
import CompositePattern.Employed;
import CompositePattern.EmployedBank;
import CompositePattern.ManagerBank;
import DecoratorPattern.CompBank;
import DecoratorPattern.Credits;
import DecoratorPattern.Deposits;
import DecoratorPattern.NameBank;
import FacadePattern.DepositsBank;

import static java.lang.System.out;

public class Main {
    public static void main(String[] args) {
        out.println("Adapter Pattern..."); // Adapter Pattern...
        GetCredit credit = new DetailBank("MobiasBank", "Student", 753, 15);
        out.println(credit.gettingCredit());
        out.print("\n");
        out.println("Composite Pattern...");
        Employed manager = new ManagerBank(100, "MobiasBank ");
        Employed employed1 = new Creditor(101, "MoldincomBank");
        Employed employed3 = new EmployedBank(102, "AgroindBank");
        manager.add(employed1);
        manager.add(employed3);
        manager.print();
        out.print("\n");
        out.println("Decorator Pattern..."); // Decorator Pattern...
        CompBank compBank = new Credits(new Deposits(new NameBank()));
        out.println(compBank.InfoBank());
        out.print("\n");
        out.println("Facade Pattern... "); // Facade Pattern...
        DepositsBank depositsBank = new DepositsBank();
        out.println(depositsBank.informJazz());
        out.println(depositsBank.informSonata());
        out.println(depositsBank.informTango());
        out.print("\n");
        out.println("Bridge Pattern..."); // Bridge Pattern...
        Currency dollar = new Converter(12, new ConvertDollar());
        Currency euro = new Converter(24, new ConvertEuro());
        Currency rubl = new Converter(48, new ConvertRubl());
        dollar.displays();
        euro.displays();
        rubl.displays();
    }
}
